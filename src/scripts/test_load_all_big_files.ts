/** Load all big non-split open data files in RAM as a test. */

import commandLineArgs from "command-line-args"

import { EnabledDatasets } from "../datasets"
import { loadAssembleeDataFromBigFiles } from "../loaders"
import { Legislature } from "../types/legislatures"

const optionsDefinitions = [
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

loadAssembleeDataFromBigFiles(
  options.dataDir,
  EnabledDatasets.All,
  Legislature.All,
  {
    log: true,
  },
)
