import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

import { datasets, DatasetStructure } from "../datasets"
import { walkDir } from "../file_systems"

const optionsDefinitions = [
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

function mergeScrutins(dataDir: string) {
  for (const dataset of datasets.scrutins) {
    switch (dataset.structure) {
      case DatasetStructure.SegmentedFiles:
        {
          const originalJsonDir: string = path.join(dataDir, dataset.filename)
          if (!options.silent) {
            console.log(`Merging ${originalJsonDir}`)
          }

          const datasetMergedFilePath = path.join(
            dataDir,
            path.basename(originalJsonDir, ".json") + "_fusionne.json",
          )

          const scrutins = []
          for (const scrutinSplitPath of walkDir(originalJsonDir)) {
            const scrutinFilename = scrutinSplitPath[scrutinSplitPath.length - 1]
            if (!scrutinFilename.endsWith(".json")) {
              continue
            }
            const scrutinOriginalFilePath = path.join(
              originalJsonDir,
              ...scrutinSplitPath,
            )
            const scrutinOriginalJson: string = fs.readFileSync(scrutinOriginalFilePath, {
              encoding: "utf8",
            })
            const scrutinOriginal: any = JSON.parse(scrutinOriginalJson)
            const scrutin = scrutinOriginal.scrutin
            scrutins.push(scrutin)
          }
          scrutins.sort((a: { uid: string }, b: { uid: string }) =>
            a.uid.length === b.uid.length
              ? a.uid.localeCompare(b.uid)
              : a.uid.length - b.uid.length,
          )

          fs.writeFileSync(
            datasetMergedFilePath,
            JSON.stringify({ scrutins: { scrutin: scrutins } }, null, 2),
            {
              encoding: "utf8",
            },
          )
        }
        break
    }
  }
}

mergeScrutins(options.dataDir)
