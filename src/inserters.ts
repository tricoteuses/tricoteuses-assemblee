import { Acteur, Organe } from "./types/acteurs_et_organes"
import { Reunion } from "./types/agendas"
import { Amendement } from "./types/amendements"
import {
  ActeLegislatif,
  Division,
  Document,
  DossierParlementaire,
} from "./types/dossiers_legislatifs"
import { Scrutin } from "./types/scrutins"

export interface OutputData {
  readonly acteurByUid?: { [uid: string]: Acteur }
  readonly amendementByUid?: { [uid: string]: Amendement }
  readonly documentByUid?: { [uid: string]: Document }
  readonly dossierByUid?: { [uid: string]: DossierParlementaire }
  readonly organeByUid?: { [uid: string]: Organe }
  readonly reunionByUid?: { [uid: string]: Reunion }
  readonly scrutinByUid?: { [uid: string]: Scrutin }
}

function insertActeLegislatifReferences(
  acte: ActeLegislatif,
  data: OutputData,
  visitedUids: Set<string>,
) {
  if (!visitedUids.has(acte.uid)) {
    visitedUids.add(acte.uid)

    if (data.organeByUid !== undefined && acte.organeRef !== undefined) {
      const organe = data.organeByUid[acte.organeRef]
      if (organe !== undefined) {
        acte.organe = organe
        insertOrganeReferences(organe, data, visitedUids)
      }
    }
    if (data.organeByUid !== undefined && acte.provenanceRef !== undefined) {
      const organe = data.organeByUid[acte.provenanceRef]
      if (organe !== undefined) {
        acte.provenance = organe
        insertOrganeReferences(organe, data, visitedUids)
      }
    }
    if (data.acteurByUid !== undefined && acte.rapporteurs !== undefined) {
      for (const rapporteur of acte.rapporteurs) {
        const acteur = data.acteurByUid[rapporteur.acteurRef]
        if (acteur !== undefined) {
          rapporteur.acteur = acteur
          insertActeurReferences(acteur, data, visitedUids)
        }
      }
    }
    if (data.reunionByUid !== undefined && acte.reunionRef !== undefined) {
      const reunion = data.reunionByUid[acte.reunionRef]
      if (reunion !== undefined) {
        acte.reunion = reunion
        insertReunionReferences(reunion, data, visitedUids)
        if (acte.odjRef !== undefined) {
          const odj = reunion.odj
          if (odj !== undefined && odj.pointsOdj !== undefined) {
            for (const pointOdj of odj.pointsOdj) {
              if (pointOdj.uid === acte.odjRef) {
                acte.odj = pointOdj
                break
              }
            }
          }
        }
      }
    }
    if (data.documentByUid !== undefined && acte.texteAdopteRef !== undefined) {
      const document = data.documentByUid[acte.texteAdopteRef]
      if (document !== undefined) {
        acte.texteAdopte = document
        insertDocumentReferences(document, data, visitedUids)
      }
    }
    if (data.documentByUid !== undefined && acte.texteAssocieRef !== undefined) {
      const document = data.documentByUid[acte.texteAssocieRef]
      if (document !== undefined) {
        acte.texteAssocie = document
        insertDocumentReferences(document, data, visitedUids)
      }
    }
    if (data.documentByUid !== undefined && acte.textesAssocies !== undefined) {
      for (const texteAssocie of acte.textesAssocies) {
        const document = data.documentByUid[texteAssocie.texteAssocieRef]
        if (document !== undefined) {
          texteAssocie.texteAssocie = document
          insertDocumentReferences(document, data, visitedUids)
        }
      }
    }
    if (data.scrutinByUid !== undefined && acte.voteRefs !== undefined) {
      const scrutins = []
      for (const voteRef of acte.voteRefs) {
        const scrutin = data.scrutinByUid[voteRef]
        if (scrutin !== undefined) {
          scrutins.push(scrutin)
          insertScrutinReferences(scrutin, data, visitedUids)
        }
      }
      acte.votes = scrutins
    }

    for (const childActe of acte.actesLegislatifs || []) {
      insertActeLegislatifReferences(childActe, data, visitedUids)
    }
  }

  return acte
}

export function insertActeurReferences(
  acteur: Acteur,
  data: OutputData,
  visitedUids: Set<string>,
) {
  if (!visitedUids.has(acteur.uid)) {
    visitedUids.add(acteur.uid)

    if (data.organeByUid !== undefined) {
      for (const mandat of acteur.mandats || []) {
        const organes = []
        for (const organeRef of mandat.organesRefs) {
          const organe = data.organeByUid[organeRef]
          if (organe !== undefined) {
            organes.push(organe)
            insertOrganeReferences(organe, data, visitedUids)
          }
        }
        mandat.organes = organes
      }
    }
  }

  return acteur
}

export function insertAmendementReferences(
  amendement: Amendement,
  data: OutputData,
  visitedUids: Set<string>,
) {
  if (!visitedUids.has(amendement.uid)) {
    visitedUids.add(amendement.uid)

    const signataires = amendement.signataires
    const auteur = signataires.auteur
    if (data.acteurByUid !== undefined && auteur.acteurRef !== undefined) {
      const acteur = data.acteurByUid[auteur.acteurRef]
      if (acteur !== undefined) {
        auteur.acteur = acteur
        insertActeurReferences(acteur, data, visitedUids)
      }
    }
    if (data.organeByUid !== undefined && auteur.groupePolitiqueRef !== undefined) {
      const organe = data.organeByUid[auteur.groupePolitiqueRef]
      if (organe !== undefined) {
        auteur.groupePolitique = organe
        insertOrganeReferences(organe, data, visitedUids)
      }
    }
    if (data.organeByUid !== undefined && auteur.organeRef !== undefined) {
      const organe = data.organeByUid[auteur.organeRef]
      if (organe !== undefined) {
        auteur.organe = organe
        insertOrganeReferences(organe, data, visitedUids)
      }
    }
    if (data.acteurByUid !== undefined) {
      const cosignatairesRefs = signataires.cosignatairesRefs
      if (cosignatairesRefs !== undefined) {
        const cosignataires = []
        for (const cosignataireRef of signataires.cosignatairesRefs || []) {
          const cosignataire = data.acteurByUid[cosignataireRef]
          if (cosignataire !== undefined) {
            cosignataires.push(cosignataire)
            insertActeurReferences(cosignataire, data, visitedUids)
          }
        }
        signataires.cosignataires = cosignataires
      }
    }

    if (data.documentByUid !== undefined) {
      const saisine = amendement.identifiant.saisine
      const texteLegislatif = data.documentByUid[saisine.texteLegislatifRef]
      if (texteLegislatif !== undefined) {
        saisine.texteLegislatif = texteLegislatif
        insertDocumentReferences(texteLegislatif, data, visitedUids)
      }
    }
  }

  return amendement
}

function insertDivisionReferences(
  division: Division,
  data: OutputData,
  visitedUids: Set<string>,
) {
  for (const auteur of division.auteurs) {
    const acteurSummary = auteur.acteur
    if (data.acteurByUid !== undefined && acteurSummary !== undefined) {
      const acteur = data.acteurByUid[acteurSummary.acteurRef]
      if (acteur !== undefined) {
        acteurSummary.acteur = acteur
        insertActeurReferences(acteur, data, visitedUids)
      }
    }

    if (data.organeByUid !== undefined && auteur.organeRef !== undefined) {
      const organe = data.organeByUid[auteur.organeRef]
      if (organe !== undefined) {
        auteur.organe = organe
        insertOrganeReferences(organe, data, visitedUids)
      }
    }
  }

  if (data.dossierByUid !== undefined) {
    const dossier = data.dossierByUid[division.dossierRef]
    if (dossier !== undefined) {
      division.dossier = dossier
      insertDossierReferences(dossier, data, visitedUids)
    }
  }

  for (const childDivision of division.divisions || []) {
    insertDivisionReferences(childDivision, data, visitedUids)
  }

  return division
}

export function insertDocumentReferences(
  document: Document,
  data: OutputData,
  visitedUids: Set<string>,
) {
  if (!visitedUids.has(document.uid)) {
    visitedUids.add(document.uid)

    for (const auteur of document.auteurs) {
      const acteurSummary = auteur.acteur
      if (data.acteurByUid !== undefined && acteurSummary !== undefined) {
        const acteur = data.acteurByUid[acteurSummary.acteurRef]
        if (acteur !== undefined) {
          acteurSummary.acteur = acteur
          insertActeurReferences(acteur, data, visitedUids)
        }
      }

      if (data.organeByUid !== undefined && auteur.organeRef !== undefined) {
        const organe = data.organeByUid[auteur.organeRef]
        if (organe !== undefined) {
          auteur.organe = organe
          insertOrganeReferences(organe, data, visitedUids)
        }
      }
    }

    if (data.dossierByUid !== undefined) {
      const dossier = data.dossierByUid[document.dossierRef]
      if (dossier !== undefined) {
        document.dossier = dossier
        insertDossierReferences(dossier, data, visitedUids)
      }
    }

    for (const division of document.divisions || []) {
      insertDivisionReferences(division, data, visitedUids)
    }
  }

  return document
}

export function insertDossierReferences(
  dossier: DossierParlementaire,
  data: OutputData,
  visitedUids: Set<string>,
) {
  if (!visitedUids.has(dossier.uid)) {
    visitedUids.add(dossier.uid)

    for (const acte of dossier.actesLegislatifs) {
      insertActeLegislatifReferences(acte, data, visitedUids)
    }

    const initiateur = dossier.initiateur
    if (initiateur !== undefined) {
      if (data.acteurByUid !== undefined) {
        for (const acteurSummary of initiateur.acteurs || []) {
          const acteur = data.acteurByUid[acteurSummary.acteurRef]
          if (acteur !== undefined) {
            acteurSummary.acteur = acteur
            insertActeurReferences(acteur, data, visitedUids)
          }
          if (acteur !== undefined && acteurSummary.mandatRef !== undefined) {
            for (const mandat of acteur.mandats || []) {
              if (mandat.uid === acteurSummary.mandatRef) {
                acteurSummary.mandat = mandat
                break
              }
            }
          }
        }
      }
      if (data.organeByUid !== undefined && initiateur.organeRef !== undefined) {
        const organe = data.organeByUid[initiateur.organeRef]
        if (organe !== undefined) {
          initiateur.organe = organe
          insertOrganeReferences(organe, data, visitedUids)
        }
      }
    }
  }

  return dossier
}

export function insertOrganeReferences(
  organe: Organe,
  data: OutputData,
  visitedUids: Set<string>,
) {
  if (!visitedUids.has(organe.uid)) {
    visitedUids.add(organe.uid)

    if (data.organeByUid !== undefined && organe.organeParentRef !== undefined) {
      const organeParent = data.organeByUid[organe.organeParentRef]
      if (organeParent !== undefined) {
        organe.organeParent = organeParent
        insertOrganeReferences(organeParent, data, visitedUids)
      }
    }

    // TODO
  }

  return organe
}

export function insertReunionReferences(
  reunion: Reunion,
  data: OutputData,
  visitedUids: Set<string>,
) {
  if (!visitedUids.has(reunion.uid)) {
    visitedUids.add(reunion.uid)

    const demandeurs = reunion.demandeurs
    if (demandeurs !== undefined) {
      if (data.acteurByUid !== undefined) {
        for (const acteurSummary of demandeurs.acteurs || []) {
          const acteur = data.acteurByUid[acteurSummary.acteurRef]
          if (acteur !== undefined) {
            acteurSummary.acteur = acteur
            insertActeurReferences(acteur, data, visitedUids)
          }
        }
      }
      if (data.organeByUid !== undefined) {
        const organeSummary = demandeurs.organe
        if (organeSummary !== undefined) {
          const organe = data.organeByUid[organeSummary.organeRef]
          if (organe !== undefined) {
            organeSummary.organe = organe
            insertOrganeReferences(organe, data, visitedUids)
          }
        }
      }
    }
    if (data.organeByUid !== undefined && reunion.organeReuniRef !== undefined) {
      const organeReuni = data.organeByUid[reunion.organeReuniRef]
      if (organeReuni !== undefined) {
        reunion.organeReuni = organeReuni
        insertOrganeReferences(organeReuni, data, visitedUids)
      }
    }
    if (data.acteurByUid !== undefined) {
      const participants = reunion.participants
      if (participants !== undefined) {
        for (const participantInterne of participants.participantsInternes || []) {
          const acteur = data.acteurByUid[participantInterne.acteurRef]
          if (acteur !== undefined) {
            participantInterne.acteur = acteur
            insertActeurReferences(acteur, data, visitedUids)
          }
        }
      }
    }
  }

  return reunion
}

export function insertScrutinReferences(
  scrutin: Scrutin,
  _data: OutputData,
  visitedUids: Set<string>,
) {
  if (!visitedUids.has(scrutin.uid)) {
    visitedUids.add(scrutin.uid)

    // TODO
  }

  return scrutin
}
