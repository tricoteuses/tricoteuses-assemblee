declare module "node-stream-zip" {
  import node_stream_zip from "node-stream-zip"

  class StreamZip {
    constructor(config: any)

    close(): any
    extract(a: any, b: any, c: any): any
    on(s: string, f: any): any
  }
  export = StreamZip
}
