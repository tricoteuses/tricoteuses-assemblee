import assert from "assert"

const booleanByString: { [key: string]: boolean } = {
  false: false,
  true: true,
}

export function cleanBooleanAttribute(
  o: { [key: string]: any },
  key: string,
): boolean | undefined {
  const stringValue = o[key]
  if (stringValue === undefined || stringValue === null) {
    return undefined
  }
  const booleanValue = booleanByString[stringValue]
  assert.notStrictEqual(
    booleanValue,
    undefined,
    `Invalid boolean value: ${key} = ${stringValue}`,
  )
  o[key] = booleanValue
  return booleanValue
}

/// Remove XML fields that have no use in JSON.
export function cleanXmlArtefacts(o: { [key: string]: any }): void {
  // Remove namespaces.

  const xmlns = o["@xmlns"]
  if (xmlns !== undefined) {
    assert.strictEqual(xmlns, "http://schemas.assemblee-nationale.fr/referentiel")
    delete o["@xmlns"]
  }

  const xmlnsXsi = o["@xmlns:xsi"]
  if (xmlnsXsi !== undefined) {
    assert.strictEqual(xmlnsXsi, "http://www.w3.org/2001/XMLSchema-instance")
    delete o["@xmlns:xsi"]
  }

  // Remove null items.

  for (const [key, value] of Object.entries(o)) {
    if (value === null || (value !== undefined && value["@xsi:nil"] === "true")) {
      delete o[key]
    }
  }
}
