import assert from "assert"

import { cleanBooleanAttribute, cleanXmlArtefacts } from "./xml"
import { AvantAApresEnum } from "../raw_types/amendements"

export function cleanAmendement(amendement: any): void {
  cleanXmlArtefacts(amendement)

  const saisine = amendement.identifiant.saisine
  const texteLegislatifRef = saisine.refTexteLegislatif
  {
    assert.notStrictEqual(texteLegislatifRef, undefined)
    delete saisine.refTexteLegislatif
    saisine.texteLegislatifRef = texteLegislatifRef
  }

  const signataires = amendement.signataires
  {
    cleanXmlArtefacts(signataires)

    const auteur = signataires.auteur
    {
      cleanXmlArtefacts(auteur)
    }

    if (signataires.cosignataires !== undefined) {
      const acteurRef = signataires.cosignataires.acteurRef
      if (Array.isArray(acteurRef)) {
        signataires.cosignatairesRefs = acteurRef
      } else {
        assert(acteurRef)
        signataires.cosignatairesRefs = [acteurRef]
      }
      delete signataires.cosignataires
    }
  }

  const pointeurFragmentTexte = amendement.pointeurFragmentTexte
  {
    cleanXmlArtefacts(pointeurFragmentTexte)

    const division = pointeurFragmentTexte.division
    {
      cleanXmlArtefacts(division)

      let avantAApres = division.avant_A_Apres
      if (avantAApres !== undefined) {
        delete division.avant_A_Apres
        if (avantAApres === AvantAApresEnum.Après) {
          avantAApres = AvantAApresEnum.Apres
        }
        division.avantAApres = avantAApres
      }
    }

    const alinea = pointeurFragmentTexte.alinea
    if (alinea !== undefined) {
      cleanXmlArtefacts(alinea)

      let avantAApres = alinea.avant_A_Apres
      if (avantAApres !== undefined) {
        delete alinea.avant_A_Apres
        if (avantAApres === AvantAApresEnum.Après) {
          avantAApres = AvantAApresEnum.Apres
        }
        alinea.avantAApres = avantAApres
      }
    }
  }

  const corps = amendement.corps
  {
    cleanXmlArtefacts(corps)

    assert.strictEqual(corps.annexeExposeSommaire, undefined)

    let dispositifAmdtCredit = corps.dispositifAmdtCredit
    if (dispositifAmdtCredit !== undefined) {
      let programme = dispositifAmdtCredit.listeProgrammes.programme
      if (Array.isArray(programme)) {
        dispositifAmdtCredit.listeProgrammes = programme
      } else {
        assert(programme)
        dispositifAmdtCredit.listeProgrammes = [programme]
      }

      for (const programme of dispositifAmdtCredit.listeProgrammes) {
        cleanXmlArtefacts(programme)

        if (programme.lignesCredits !== undefined) {
          let ligneCredit = programme.lignesCredits.ligneCredit
          if (Array.isArray(ligneCredit)) {
            programme.lignesCredits = ligneCredit
          } else {
            assert(programme)
            programme.lignesCredits = [ligneCredit]
          }
        }
      }
    }

    const avantAppel = corps.avantAppel
    if (avantAppel !== undefined) {
      cleanXmlArtefacts(avantAppel)

      assert.notStrictEqual(avantAppel.dispositif, undefined)
    }
  }

  let representation = amendement.representations.representation
  if (Array.isArray(representation)) {
    amendement.representations = representation
  } else {
    assert(representation)
    amendement.representations = [representation]
  }
  for (const representation of amendement.representations) {
    cleanXmlArtefacts(representation)

    assert.strictEqual(representation.repSource, undefined)
    assert.strictEqual(representation.offset, undefined)
    assert.strictEqual(representation.dateDispoRepresentation, undefined)

    const statutRepresentation = representation.statutRepresentation
    {
      cleanBooleanAttribute(statutRepresentation, "canonique")
      cleanBooleanAttribute(statutRepresentation, "enregistrement")
      cleanBooleanAttribute(statutRepresentation, "officielle")
      cleanBooleanAttribute(statutRepresentation, "transcription")
      cleanBooleanAttribute(statutRepresentation, "verbatim")
    }
  }

  const sort = amendement.sort
  if (sort !== undefined) {
    cleanXmlArtefacts(sort)
  }

  const loiReference = amendement.loiReference
  {
    cleanXmlArtefacts(loiReference)
  }
}

export function cleanTexteLegislatif(texteLegislatif: any): void {
  cleanXmlArtefacts(texteLegislatif)

  // Note: The following fields in `texteLegislatif.amendements` are currently ignored:
  // * "#text"?:       string;
  // * offset?:        null;
  // * nom?:           Nom;
  // * avant_A_Apres?: DivisionAvantAApres;
  // * dateDepot?:     Date;
  // * documentURI?:   string;
  // Because it seems they should belong to an "amendement".
  let amendement = texteLegislatif.amendements.amendement
  if (Array.isArray(amendement)) {
    texteLegislatif.amendements = amendement
  } else {
    assert(amendement)
    texteLegislatif.amendements = [amendement]
  }

  for (const amendement of texteLegislatif.amendements) {
    cleanAmendement(amendement)
  }
}
