import { uncapitalizeFirstLetter } from "./strings"
import { CodeTypeOrgane } from "./types/acteurs_et_organes"
import { DossierParlementaire, ActeLegislatif } from "./types/dossiers_legislatifs"

export enum FinalStatus {
  Failure = "FAILURE",
  Success = "SUCCESS",
}

interface State {
  currentStatus: Status
  finalStatus?: FinalStatus
  label: string
}

export type Status = CodeTypeOrgane | StatusOnly

export enum StatusOnly {
  Failure = "FAILURE",
  InProgress = "IN_PROGRESS",
  Success = "SUCCESS",
}

export function statusFromCodierLibelle(libelle: string): Status {
  switch (libelle) {
    case "Accord":
    case "adopté":
    case "adopté, dans les conditions prévues à l'article 45, alinéa 3," +
      " de la Constitution":
    case "adoptée":
    case "adopté sans modification":
    case "adoptée sans modification":
    case "Conforme":
    case "considérée comme définitive en application de l'article 151-7 du Règlement":
    case "considérée comme définitive en application de l'article 151-9 du Règlement":
    case "Motion adopté(e)":
      return StatusOnly.Success
    case "Désaccord":
    case "Motion rejeté(e)":
    case "rejeté":
    case "rejetée":
      return StatusOnly.Failure
    case "adopté avec modifications":
    case "adoptée avec modifications":
    case "De droit (article 61 alinéa 1 de la Constitution)":
    case "modifié":
    case "modifiée":
    case "Motion de censure 49-2":
    case "Partiellement conforme":
      return StatusOnly.InProgress
    case "Soixante députés au moins":
    case "Soixante sénateurs au moins":
      return CodeTypeOrgane.Constitu
    default:
      return StatusOnly.InProgress
  }
}

export function stateFromActePath(
  dossier: DossierParlementaire,
  actePath: ActeLegislatif[],
): State | null {
  const leafActe = actePath[actePath.length - 1]
  let libelle = null
  switch (leafActe.xsiType) {
    case "Adoption_Europe_Type":
      libelle = leafActe.statutAdoption!.libelle
      return {
        currentStatus: statusFromCodierLibelle(libelle),
        label: uncapitalizeFirstLetter(libelle),
      }
    case "ConclusionEtapeCC_Type":
      libelle = leafActe.statutConclusion!.libelle
      return {
        currentStatus: statusFromCodierLibelle(libelle),
        label: uncapitalizeFirstLetter(libelle),
      }
    case "Decision_Type": {
      libelle = leafActe.statutConclusion!.libelle
      let currentStatus = statusFromCodierLibelle(libelle)
      let finalStatus = undefined
      switch (dossier.procedureParlementaire.code) {
        case "PROCEDURE_PARLEMENTAIRE_2":
          // Proposition de loi ordinaire
          // Not sure for other cases => To improve.
          if (
            ["rejeté", "rejetée"].includes(libelle) &&
            dossier.actesLegislatifs.length === 1
          ) {
            finalStatus = FinalStatus.Failure
          }
          break
        case "PROCEDURE_PARLEMENTAIRE_8": // Résolution
        case "PROCEDURE_PARLEMENTAIRE_22":
          // Résolution Article 34-1
          if (currentStatus === StatusOnly.Success) {
            finalStatus = FinalStatus.Success
          } else if (currentStatus === StatusOnly.Failure) {
            finalStatus = FinalStatus.Failure
          }
          break
        default:
          break
      }
      return {
        currentStatus,
        finalStatus,
        label: uncapitalizeFirstLetter(libelle),
      }
    }
    case "DecisionMotionCensure_Type":
      libelle = leafActe.decision!.libelle
      return {
        currentStatus: statusFromCodierLibelle(libelle),
        finalStatus:
          // Not sure for other cases => To improve.
          // Engagement de la responsabilité gouvernementale
          dossier.procedureParlementaire.code === "PROCEDURE_PARLEMENTAIRE_13" &&
          libelle === "Motion rejeté(e)"
            ? FinalStatus.Failure
            : FinalStatus.Success, // TODO: Not sure of this `true` value
        label: uncapitalizeFirstLetter(libelle),
      }
    case "DecisionRecevabiliteBureau_Type":
      libelle = leafActe.decision!.libelle
      return {
        currentStatus: statusFromCodierLibelle(libelle),
        label: uncapitalizeFirstLetter(libelle),
      }
    case "DeclarationGouvernement_Type":
      libelle = leafActe.typeDeclaration!.libelle
      return {
        currentStatus: statusFromCodierLibelle(libelle),
        label: uncapitalizeFirstLetter(libelle),
      }
    case "DepotMotionCensure_Type":
      libelle = leafActe.typeMotionCensure!.libelle
      return {
        currentStatus: statusFromCodierLibelle(libelle),
        label: uncapitalizeFirstLetter(libelle),
      }
    case "DepotRapport_Type":
      if (
        [
          "DossierCommissionEnquete_Type",
          "DossierMissionControle_Type",
          "DossierMissionInformation_Type",
        ].includes(dossier.xsiType!)
      ) {
        return {
          currentStatus: actePath[0].organe!.codeType,
          finalStatus: FinalStatus.Success,
          label: "rapport déposé",
        }
      }
      return null
    case "MotionProcedure_Type":
      libelle = leafActe.typeMotion!.libelle
      return {
        currentStatus: statusFromCodierLibelle(libelle),
        label: uncapitalizeFirstLetter(libelle),
      }
    case "Promulgation_Type":
      return {
        currentStatus: StatusOnly.Success,
        finalStatus: FinalStatus.Success,
        label: "promulgué",
      }
    case "RetraitInitiative_Type":
      return {
        currentStatus: StatusOnly.Failure,
        finalStatus: FinalStatus.Failure,
        label: "retiré",
      }
    case "SaisineConseilConstit_Type":
      libelle = leafActe.casSaisine!.libelle
      return {
        currentStatus: statusFromCodierLibelle(libelle),
        label: uncapitalizeFirstLetter(libelle),
      }
    default:
      // This acte gives no status.
      return null
  }
}

export function stateFromActes(
  dossier: DossierParlementaire,
  actePath: ActeLegislatif[],
  actes: ActeLegislatif[],
): State | null {
  for (const acte of [...actes].reverse()) {
    const status = acte.actesLegislatifs
      ? stateFromActes(dossier, [...actePath, acte], acte.actesLegislatifs)
      : stateFromActePath(dossier, [...actePath, acte])
    if (status !== null) {
      return status
    }
    // No status found, continue with previous acte.
  }
  return null
}

export function stateFromDossier(dossier: DossierParlementaire): State {
  const status = stateFromActes(dossier, [], dossier.actesLegislatifs)
  if (status !== null) {
    return status
  }
  return {
    currentStatus: StatusOnly.InProgress,
    label: "en cours",
  }
}
